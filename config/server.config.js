const paths = require('@config/paths')

module.exports = {
  watchedFiles: [
    paths.ownServer,
    paths.ownConfig,
    paths.ownHtmlTemplate,
    `${paths.appPath}/components`,
    `${paths.appPath}/component.config.js`
  ]
}
