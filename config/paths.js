const path = require("path");
const fs = require("fs");

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);
const resolveOwn = relativePath => path.resolve(__dirname, "../", relativePath);

const envPublicUrl = process.env.PUBLIC_URL;
const namespace = process.env.npm_package_config_namespace;

if (!namespace) {
  console.error("Service namespace is not defined")
  process.exit(1)
}

module.exports = {
  appPath: resolveApp("."),
  appBuild: resolveApp("build"),
  appNodeModules: resolveApp("node_modules"),
  appPackageJson: resolveApp("package.json"),
  clientBuild: resolveApp("build/client"),
  clientManifest: resolveApp("build/client/manifest.json"),
  componentConfig: resolveApp("component.config"),
  serverBuild: resolveApp("build/server"),
  serverManifest: resolveApp("build/server/manifest.json"),
  ownNodeModules: resolveOwn("node_modules"),
  ownServer: resolveOwn("server"),
  ownConfig: resolveOwn("config"),
  ownHtmlTemplate: resolveOwn("templates/htmlTemplate.js")
};
