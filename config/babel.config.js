const paths = require('@config/paths')

const ownPlugin = name => `${paths.ownNodeModules}/${name}`

module.exports = {
  "plugins": [
    ownPlugin('babel-plugin-styled-components'),
    ownPlugin('babel-plugin-lodash')
  ],
  "presets": [
    [
      require.resolve("@babel/preset-env"),
      {
        targets: {
          node: "current"
        }
      }
    ],
    require.resolve("@babel/preset-react")
  ],
}
