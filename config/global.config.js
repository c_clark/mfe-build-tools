let globalConfig = global.MFE_GLOBAL_CONFIG;

const helper = overrideConfig => {
  return Object.assign({
    UI_ENDPOINT: ''
  }, overrideConfig || {})
};

const configure = overrideConfig => {
  if (!globalConfig) {
    //console.log(" -------------------- CONFIGURING ------------------- ");
    globalConfig = helper(overrideConfig)
  }
  global.MFE_GLOBAL_CONFIG = globalConfig
  return globalConfig
}

const getGlobalConfig = () => {
  if (globalConfig) return globalConfig;
  else throw new Error('Frontends not configured, please call mfe-config.configure')
}

module.exports = getGlobalConfig
module.exports.configure = configure
