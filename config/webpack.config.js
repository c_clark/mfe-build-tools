const path = require("path");
const WebpackAssetsManifest = require("webpack-assets-manifest");
const webpackNodeExternals = require("webpack-node-externals");
const StatsPlugin = require("stats-webpack-plugin");
const webpack = require("webpack");

const paths = require("./paths");
const componentConfig = require(paths.componentConfig);

const namespace = process.env.npm_package_config_namespace;

const DEVELOPMENT_MODE = mode => mode === "development";

/*function clientEntries(componentConfig) {
  let entries = {}
  Object.keys(componentConfig).forEach(entryKey =>
    entries[`${namespace}-${entryKey}`] = componentConfig[entryKey])
  return entries
}*/

module.exports = function configHelper(server, mode, forceReact) {
  return {
    name: server ? "server" : "client",
    mode: mode,
    target: server ? "node" : "web",
    context: paths.appPath,
    entry: server ? componentConfig : Object.assign({}, componentConfig, {
      'react': 'react',
      'react-dom': 'react-dom',
      'styled-components': 'styled-components'
    }),
    externals: server
      ? [
          /*webpackNodeExternals({
        modulesDir: paths.ownNodeModules
      }),*/ // For build-tools directory
          webpackNodeExternals({
            modulesDir: paths.appNodeModules
          })
        ]
      : forceReact
      ? {}
      : {
          "react-dom": ['__mfe_lib__', 'COMMON', 'react-dom'],
          react: ['__mfe_lib__', 'COMMON', 'react'],
          "styled-components": ['__mfe_lib__', 'COMMON', 'styled-components'],
          "@config/global.config": ['__mfe_lib__', 'COMMON', 'CONFIG']
        },
    output: {
      path: `${paths.appBuild}/${server ? "server" : "client"}`,
      filename: DEVELOPMENT_MODE(mode) ? "[name].js" : "[chunkhash].js",
      chunkFilename: DEVELOPMENT_MODE(mode) ? "[name].js" : "[chunkhash].js",
      library: server ? undefined : ["__mfe_lib__", `${namespace}`, "[name]"],
      libraryTarget: server ? "commonjs2" : "global",
      publicPath: "/"
    },
    optimization: {
      splitChunks: server
        ? undefined
        : {
            chunks: "all",
            /*cacheGroups: {
              "MFE_COMMON": {
                test: /[\\/][react][-dom]?/,
                enforce: true
              }
            }*/
          },
      runtimeChunk: server
        ? undefined
        : {
            name: 'runtime'
          }
    },
    recordsPath: path.resolve(paths.appPath, './records.json'),
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader",
          options: require("./babel.config")
        }
      ]
    },
    resolve: {
      modules: [paths.ownNodeModules, "node_modules"],
      alias: {
        "mfe-loader": path.resolve(__dirname, "../components/mfe-loader")
      }
    },
    resolveLoader: {
      modules: [paths.ownNodeModules, "node_modules"]
    },
    plugins: [
      new webpack.DefinePlugin({
        IS_SERVER: JSON.stringify(server),
        USE_NGINX: JSON.stringify(false)
      }),
      new WebpackAssetsManifest({
        entrypoints: true
      }),
      new StatsPlugin("stats.json", {})
    ]
  };
};
