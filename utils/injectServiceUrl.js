const globalConfig = require("@config/global.config")();

let injectServiceUrl = (service, path) => {
  if (path.startsWith("http")) return path;
  let base = globalConfig && globalConfig[`SERVICE_${service.toUpperCase()}_URL`] || ''
  return `${base}/${path}`;
};

module.exports = injectServiceUrl;
