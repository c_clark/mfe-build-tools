const USE_NGINX = false;

const loadjs = require('fs').readFileSync(require.resolve('loadjs/dist/loadjs.min'))

const globalStyles = `
  * {
    font-family: Arial;
    box-sizing: border-box;
  }

  body {
    margin: 0px;
  }
`

function joinScriptArray(scripts) {
  if (scripts.length > 0)
    return `'${scripts.join('\',\'')}',`
}

const appNamespace = process.env.npm_package_config_namespace

function template(appMarkup, commonChunks, componentChunks, component) {
  let renderQueue = global.__MFE_RENDER_CONTEXT__ && global.__MFE_RENDER_CONTEXT__.renderQueue || []

  return `
      <html>
        <head>
          ${
            USE_NGINX ? // Production nginx => output SSI tag to embed runtime
            null :
            (
              '<script src="/runtime.js"></script>'
            )
          }
          <script>${loadjs}</script>
          <script>
            loadjs([
              // APP COMMON CHUNKS
              ${joinScriptArray(componentChunks.load) || ``}
              // COMMON CHUNKS
              ${joinScriptArray(commonChunks.load.concat(commonChunks.after)) || ``}
            ], {
              async: false,
              success: function() {
                let commons = window.__mfe_lib__['${appNamespace}']
                window.__mfe_lib__['COMMON'] = {
                  'CONFIG': function() { return ${JSON.stringify(global.MFE_GLOBAL_CONFIG)}; },
                  'react': commons.react,
                  'react-dom': commons['react-dom'],
                  'styled-components': commons['styled-components']
                }

                loadjs([
                  // RENDER QUEUE LOAD
                  ${joinScriptArray(renderQueue.load) || ``}
                  // RENDER QUEUE AFTER
                  ${joinScriptArray(renderQueue.after) || ``}
                  // COMPONENT ENTRY
                  ${joinScriptArray(componentChunks.after) || `` }
                ], {
                  async: false, // Execute in series to ensure dependencies are loaded
                  // Still fetches in parallel async due to script injection
                  success: function() {

                    let commons = window.__mfe_lib__['${appNamespace}']
                    commons['react-dom'].hydrate(
                      commons['react'].createElement(commons['${component}'].default),
                      document.querySelector('#app')
                    )
                  },
                  error: function (pathsNotFound) { console.log(pathsNotFound); }
                })
              },
              error: function (pathsNotFound) { console.log(pathsNotFound); }
            })
          </script>
          <style>
            ${globalStyles}
          </style>
        </head>
        <body>
          <div id="app">${appMarkup}</div>
        </body>
      </html>
    `;
}

module.exports = template
