const serveComponentStatic = require('../routes/serveComponentStatic')
const serveComponentChunks = require('../routes/serveComponentChunks')

module.exports = ({ clientManifest, components }) => ({
  serveComponentStatic: serveComponentStatic({ clientManifest, components }),
  serveComponentChunks: serveComponentChunks({ clientManifest, components })
})
