const serveComponentStatic = ({ clientManifest, components }) => {
  const bootstrapAndRender = require("../utils/bootstrapAndRender");
  const htmlTemplate = require('../../templates/htmlTemplate');

  const commonEntries = [ 'react', 'react-dom', 'styled-components' ];
  const chunksToServe = require('../utils/chunksToServe')(clientManifest.entrypoints)

  function setupThisComponentChunks(component) {
    const componentChunks = chunksToServe[component].js
    const allComponentChunks = clientManifest.entrypoints[component].js

    const concatAndFilter = (list, newChunks) =>
      list.concat(newChunks)
        .filter(chunk => !allComponentChunks.includes(chunk))

    const commonChunks = commonEntries.reduce((chunkList, entry) =>
      ['before', 'load', 'after'].reduce((list, key) => ({
        ...list,
        [key]: concatAndFilter(list[key] || [], chunksToServe[entry].js[key])
      }), chunkList), {})

    const addSlash = chunks => Object.keys(chunks).reduce((list, key) => ({
        ...list,
        [key]: chunks[key].map(chunk => `/${chunk}`)
      }), {})

    return {
      commonChunks: addSlash(commonChunks),
      componentChunks: addSlash(componentChunks)
    }
  }

  return (req, res) => {
    // This route is only ever called during SSR, perhaps should protect it with
    // an auth mechanism, e.g. key-exchange
    let { component } = req.params;
    let { props, template } = req.query;

    bootstrapAndRender({ component, components })
      .then(({ markup }) => {
        if (template === '' || template === 'true' || template === '1') {
          const { commonChunks, componentChunks } = setupThisComponentChunks(component)
          res.send(htmlTemplate(markup, commonChunks, componentChunks, component))
        } else {
          res.send(markup);
        }
      });
  }
};

module.exports = serveComponentStatic;
