const serveComponentChunks = ({ clientManifest, components }) => {
  const bootstrapAndRender = require("../utils/bootstrapAndRender");

  const clientEntries = clientManifest.entrypoints;

  return (req, res) => {
    const { component } = req.params;

    // Check if context contains chunks, if it does then server them, otherwise fetch
    // or just always fetch because we're not bothering with caching for this demo

    let chunksToServe = require("../utils/chunksToServe")(clientEntries);

    bootstrapAndRender({ component, components })
      .then(({ context }) => {
        chunksToServe[component].js.load = context.chunkQueue.load.concat(
          chunksToServe[component].js.load
        );
        chunksToServe[component].js.after = context.chunkQueue.after.concat(
          chunksToServe[component].js.after
        );

        res.json(chunksToServe[component]);
      });
  }
};

module.exports = serveComponentChunks;
