const react = require("react");
const reactDOM = require("react-dom/server");

const injectServiceUrl = require("@utils/injectServiceUrl");

const asyncBootstrapper = require("../utils/asyncBootstrapper");

const bootstrapAndRender = ({ component, components }) => {
  let context = {
    components: {},
    chunks: {},
    chunkQueue: {
      before: [],
      load: [],
      after: []
    },
    get renderQueue() {
      return {
        before: this.chunkQueue.before,
        load: this.chunkQueue.load,
        after: this.chunkQueue.after,
        push: ({ service, component }) => {
          if (
            this.chunks &&
            this.chunks[service] &&
            this.chunks[service][component]
          ) {
            let queues = ["before", "load", "after"];
            queues.forEach(queue => {
              this.chunkQueue[queue] = this.chunkQueue[queue].concat(
                this.chunks[service][component]["js"][queue].map(file =>
                  injectServiceUrl(service, file)
                ) || []
              );
            });
          }
        }
      };
    }
  };

  global.__MFE_RENDER_CONTEXT__ = context;
  let root = react.createElement(components[component]);

  return asyncBootstrapper(root, {}, context).then(() => {
    let rendered = reactDOM.renderToStaticMarkup(root);

    return Promise.resolve({ markup: rendered, context });
  });
};

module.exports = bootstrapAndRender;
