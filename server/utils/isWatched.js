const watchedFiles = require('@config/server.config').watchedFiles

const isWatched = id =>
  watchedFiles.findIndex(file => id.startsWith(file)) !== -1

module.exports = isWatched
