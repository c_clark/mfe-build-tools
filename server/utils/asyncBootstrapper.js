const treeWalker = require('react-tree-walker')

function asyncBootstrapper(app, options, context = {}) {
  const visitor = (element, instance) => {
    if (
      instance &&
        typeof instance.bootstrap === 'function'
    ) {
      return instance.bootstrap()
    }
    return undefined
  }

  return treeWalker(
    app,
    visitor,
    context,
    options,
  )
}

module.exports = asyncBootstrapper
