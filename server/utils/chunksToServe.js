const getChunksToServe = clientEntries => {
  let chunksToServe = {}

  Object.keys(clientEntries).forEach(key => {
    chunksToServe[key] = {
      js: {
        before: clientEntries[key]['js'].slice(0, 1), // Runtime chunk is always first
        load: clientEntries[key]['js'].slice(1, -1), // All other chunks
        after: clientEntries[key]['js'].slice(-1) // Entry chunk is always last
      },
      assets: Object.keys(clientEntries[key]).map(type => type === 'js' ? null :
        { type, files: clientEntries[key][type] }).filter(i => i)
    }
  })

  return chunksToServe;
}

module.exports = getChunksToServe;
