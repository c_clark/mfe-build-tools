const paths = require('@config/paths')

const prepareComponents = (serverEntries, require = require) => {
  let serverEntriesList = Object.keys(serverEntries)

  let components = {}
  serverEntriesList.forEach(entry => {
    let fileList = serverEntries[entry]
    let jsFiles = fileList['js']

    jsFiles.forEach((jsFile, i) => {
      // This is not strictly necessary, as all chunks are bundled into one file
      // for the server, but this is not necessarily great for memory usage...
      if (i === (jsFiles.length - 1)) {
        components[entry] = require(`${paths.serverBuild}/${jsFile}`).default
      }// else require(jsFile)
    })
  })

  return components;
}

module.exports = prepareComponents
