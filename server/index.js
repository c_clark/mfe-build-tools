const start = require('./scripts/start')
const restart = require('./scripts/restart')

module.exports = {
  start,
  restart
}
