const webpack = require("webpack");

const paths = require("@config/paths");
const configFactory = require("@config/webpack.config");

const appName = require(paths.appPackageJson).name;

let clientConfig = configFactory(false, "development", appName === 'service-app');
let serverConfig = configFactory(true, "development");

const expressFactory = require("../factories/express");

const port =
  process.env.npm_package_config_port ||
  3000;

const startServer = () => {
  let compiler = webpack([clientConfig, serverConfig]);
  return expressFactory(true, compiler).listen(port, () => {
    console.log(`${appName} listening on port ${port}`);
  });
};

module.exports = startServer;
