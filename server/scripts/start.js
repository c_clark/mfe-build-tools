const chokidar = require('chokidar')

const startServer = require('./startServer')
const restart = require('./restart')

let server = null;

let watchedFiles = require('@config/server.config').watchedFiles

const start = watch => {
  // Launch WebpackDevServer
  server = startServer()

  if (watch) {
    chokidar.watch(watchedFiles)
      .on('all', (event, at) => {
        if (event === 'add') {
          console.log(`File added: ${at}`);
        }

        if (event === 'change') {
          console.log(`File changed: ${at}`);
          restart(server).then(newServer => server = newServer);
        }
      })
  }
}

module.exports = start
