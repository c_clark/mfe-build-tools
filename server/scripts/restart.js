const startServer = require('./startServer')
const isWatched = require('../utils/isWatched')

const restart = server => {
  // clean cache
  Object.keys(require.cache).forEach(id => {
    if (isWatched(id)) {
      console.log(`Reloading ${id}`);
      delete require.cache[id];
    }
  })

  return new Promise((resolve, reject) => {
    server.close(() => {
      console.log(`Server is shutdown`);
      console.log(`-------- Restarting --------`);
      resolve(startServer());
    })
  });
}

module.exports = restart
