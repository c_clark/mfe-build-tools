const express = require('express')

const routeFactory = ({ clientManifest, serverManifest, components }) => {
  const router = express.Router();
  const routes = require('../routes')({ clientManifest, components });

  const serverEntries = Object.keys(serverManifest.entrypoints);

  // Middleware to protect against requests for components that don't exist
  router.use('/:component', (req, res, next) => {
    if (!serverEntries.includes(req.params.component)) {
      return res.sendStatus(404)
    }
    next();
  })

  // Handler to serve static markup for requested component
  router.get('/:component/static', routes.serveComponentStatic)

  // Handler to list required chunks for requested component
  router.get('/:component', routes.serveComponentChunks)

  return router;
}

module.exports = routeFactory
