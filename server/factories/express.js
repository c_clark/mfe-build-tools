const express = require('express')
const cors = require('cors')

const paths = require('@config/paths')

const routeFactory = require('./routes')

const prepareComponents = require('../utils/prepareComponents')

module.exports = function expressFactory(useDevServer, compiler) {
  const app = express()

  // Needed when using express to serve frontends from different ports on
  // localhost to ensure no issues with request resolution
  app.use(cors())

  // To stop annoying requests for favicon
  app.get('/favicon.ico', (req, res) => res.sendStatus(204))

  if (useDevServer) {
    if (!compiler) {
      console.error("Trying to use webpack-dev-middleware but no compiler provided")
      process.exit(1)
    } else {
      // Serving from memory-fs
      const webpackDevMiddleware = require('webpack-dev-middleware')
      const middlewareInstance = webpackDevMiddleware(compiler, {
        publicPath: '/',
        // Use this to see what the files are like if needed
        // Files will always be served from memory
        // writeToDisk: true
      })

      const readMemoryFile = path =>
        middlewareInstance.fileSystem.readFileSync(path)

      const readManifestFromMemory = path =>
        JSON.parse(readMemoryFile(path).toString())

      // TODO: Investigate if worth using something like this instead of eval
      /*const requireFromMemoryFs = path => {
        const vm = require('vm');
        const script = new vm.Script(readMemoryFile(path))
        script.runInThisContext();
      }*/

      const memoryRequire = path => eval(readMemoryFile(path).toString())

      middlewareInstance.waitUntilValid(() => {
        app.use(routeFactory({
          serverManifest: readManifestFromMemory(paths.serverManifest),
          clientManifest: readManifestFromMemory(paths.clientManifest),
          components: prepareComponents(
            readManifestFromMemory(paths.serverManifest).entrypoints,
            memoryRequire
          )
        }))
      })

      app.use(middlewareInstance)
    }
  } else {
    app.use('/', express.static(paths.clientBuild, {
      index: false,
      //maxAge: '24h',
    }))

    app.use(routeFactory({
      serverManifest: require(paths.serverManifest),
      clientManifest: require(paths.clientManifest),
      components: prepareComponents(require(paths.serverManifest).entrypoints)
    }))
  }

  return app
}
