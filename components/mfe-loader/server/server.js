import React, { Component } from 'react';
import PropTypes from 'prop-types'
import parse from 'html-react-parser'

import _ from 'lodash'

import {
  getComponentStaticMarkup,
  getComponentChunks
} from '../utils/fetchUtils'

// NOTE: 2 Step Process required for render:
//    1. Retrieve static markup for component, render and return
//    2. Retrieve list of required chunks, pass to global chunk requires

export default class MFEServer extends Component {
  async bootstrap() {
    let { service, component } = this.props
    let context = this.context || (this.context = {})

    // 1. Retrieve static markup for component and store in render context
    const staticMarkup = await getComponentStaticMarkup({ service, component });
    _.set(context, ['components', service, component], staticMarkup);

    // 2. Retrieve list of required chunks
    const chunks = await getComponentChunks({ service, component });
    _.set(context, ['chunks', service, component], chunks);

    // Push current component onto render queue
    let renderQueue = (global.__MFE_RENDER_CONTEXT__ && global.__MFE_RENDER_CONTEXT__.renderQueue) || []
    renderQueue.push({ service, component })

    // Resolve with false to stop the tree walker continuing
    // We can guarantee that there is no more bootstrapping to be done due to
    // passing static markup around on server side
    return Promise.resolve(false)
  }

  render() {
    const { service, component } = this.props
    let components = global.__MFE_RENDER_CONTEXT__ && global.__MFE_RENDER_CONTEXT__.components

    let markup = (components && components[service]
      && components[service][component]) || 'Failed'

    // TODO:
    // If using nginx, return SSI virtual tag linked to static endpoint of component
    // If not using nginx, return fragment with static markup fetched synchronously
    return parse(markup)

  }
}

MFEServer.propTypes = {
  service: PropTypes.string,
  component: PropTypes.string,
}
