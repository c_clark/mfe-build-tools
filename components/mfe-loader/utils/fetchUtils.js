import loadjs from 'loadjs'
import axios from 'axios'

import injectServiceUrl from '@utils/injectServiceUrl'

const getComponentStaticMarkup = async ({service, component}) => {
  return await axios.get(injectServiceUrl(service, `${component}/static`))
    .then(response => response.data)
}

const getComponentChunks = async ({ service, component }) => {
  return await axios.get(injectServiceUrl(service, `${component}`))
    .then(response => response.data)
}

const loadClientComponent = async ({ service, component }) => {
  let componentChunks = await getComponentChunks({ service, component });

  // No before key here, as we assume the runtime is loaded already
  let flatChunks = [/*'before',*/ 'load', 'after'].reduce((flat, key) => flat.concat(componentChunks[key]), [])

  loadjs(flatChunks.map(chunk => injectServiceUrl(service, chunk)), {
    async: false, // Load in series to ensure dependencies are loaded
    success: () => {
      console.log(`Successfully loaded ${service} ${component}`);
      Promise.resolve()
      // This is where the mount callback goes (if needed)
    },
    error: pathsNotFound => console.error(pathsNotFound)
  })
}

export { loadClientComponent, getComponentStaticMarkup, getComponentChunks }
