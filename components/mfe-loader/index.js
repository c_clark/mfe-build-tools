import client from './client'
import server from './server'

let isServer = !!IS_SERVER

export default isServer ? server : client
