import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { loadClientComponent } from '../utils/fetchUtils'

export default class MFEClient extends Component {
  constructor(props) {
    super(props);

    let content = global.__mfe_lib__[props.service][props.component].default
    this.state = {
      loading: !content,
      content: content || (() => {})
    };
  }

  componentDidMount() {
    let { service, component } = this.props

    if (!__mfe_lib__[service][component]) {
      loadClientComponent({ service, component })
        .then(() => {
          this.setState({
            loading: false,
            content: window.__mfe_lib__[service][component].default
          })
        })
    }
  }

  render() {
    let Content = this.state.content

    return this.state.loading ? (
      <div>Loading...</div>
    ) : (
      <Content />
    );
  }
}

MFEClient.propTypes = {
  service: PropTypes.string,
  component: PropTypes.string,
}
