process.env.BABEL_ENV = 'development'
process.env.NODE_ENV = 'development'

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

// const webpack = require('webpack')
//const WebpackDevServer = require('webpack-dev-server')

const paths = require('../config/paths')
// const configFactory = require('../config/webpack.config')
//const createDevServerConfig = require('../config/webpackDevServer.config')

// let clientConfig = configFactory(false, "development")
// let serverConfig = configFactory(true, "development")
//
// let compiler = webpack([clientConfig, serverConfig])

//const expressFactory = require('../config/express')

//let server = expressFactory(true, compiler)

//console.log(compiler);

// Warn and crash if required files are missing
//if (!checkRequiredFiles([paths.appHtml, paths.appIndexJs])) {
//  process.exit(1);
//}

const DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3000;
const HOST = process.env.HOST || '0.0.0.0';

const appName = require(paths.appPackageJson).name

// Set console title to avoid confusion
if (process.stdout.isTTY) {
  const setTerminalTitle = require('../utils/setTerminalTitle')
  setTerminalTitle(process.env.MFE_TERMINAL_TITLE || appName || "Micro-Frontend")
}

//const serverConfig = createDevServerConfig(proxyConfig, urls.lanUrlForConfig);

//const devServer = new WebpackDevServer(compiler, serverConfig)

const server = require('../server')
const watchServer = true

server.start(watchServer);
